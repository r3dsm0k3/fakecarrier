package com.zkhq.android;

/*
 * Hello,Source Code Reader. :)
 *
 * 1.Better UI
 * 2.Icon
 * 3.Better Error/Exception Handling
 * 4.Submit bug to deveoper option (with logcat output if possible)
 * 5.Remove Log messages
 * 6.Move to SDCard
 */
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class FakeCarrier extends Activity {
	
	public ProgressDialog dialog;
	public SharedPreferences myPrefs;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Button set_btn = (Button)findViewById(R.id.btn_set);
        set_btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(checkIfBackupExists())
					backupMyConfig();
				else
					Log.d("backup not found", "No backup");
				
				EditText desiredNameTxt = (EditText)findViewById(R.id.hack_name_txt);
				String desiredName = desiredNameTxt.getText().toString();
				hackNow(desiredName);
			}
		});
        Button reset_btn = (Button)findViewById(R.id.btn_reset);
        reset_btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				restoreMyConfig();
			}
		});
        
        myPrefs = this.getSharedPreferences("myPrefs", MODE_PRIVATE);
        
        if(myPrefs.getBoolean("rooted", false)){
        	//This dialogue is just a fancy one,not needed
	        this.dialog = ProgressDialog.show(FakeCarrier.this, "", "Ninja is checking if we are root. Hold it tight..", true);
	    	this.dialog.show();
	    	
	    	Handler handler = new Handler();
	    	handler.postDelayed(new Runnable() {
				@Override
				public void run() {
		
					SharedPreferences.Editor prefsEditor = myPrefs.edit();
		            prefsEditor.putBoolean("rooted", FakeCarrier.isDeviceRooted());
		            prefsEditor.commit();
		            dialog.dismiss(); 
				}
			}, 5000);
        }
        
        initToolBox();
        
    }
    
    
    void initToolBox(){
    	try
        {
        	if(FakeCarrier.isDeviceRooted()){
        		String[] cmd = {""};
        		this.runAsRoot(cmd);
        		
        		//IM NOT SURE WHETHER THIS WORKS WITH CDMA
        		//persist.radio.nitz_long_ons_0 is also another option,so far we're good with gsm.sim,let the bugs come and we'll decide
        		
        		InputStream stdOperatorIn = Runtime.getRuntime().exec("getprop gsm.sim.operator.alpha").getInputStream();
        		InputStreamReader stdInputStreamReader = new InputStreamReader(stdOperatorIn);
        		String orig_operator = new BufferedReader(stdInputStreamReader).readLine();
        		
        		//Make UI Changes from here.
        		TextView origTxtView = (TextView)findViewById(R.id.orig_txt);
        		origTxtView.setText("Original Name is : "+orig_operator);
        		
                 
        	}else{
        		AlertDialog exitDialog = new AlertDialog.Builder(FakeCarrier.this)
                .setTitle("Sorry dude,your device is not Rooted.!")
                .setMessage("Remember,we told you earlier?\n" +
                		"This will only work on a rooted phone,\n" +
                		"Screw warranty,root your phone and come back." +
                		"As of now you have NO options,but to Exit.! We're Sorry.!!")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                   	 Intent intent = new Intent(Intent.ACTION_MAIN);
                   	 intent.addCategory(Intent.CATEGORY_HOME);
                   	 intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   	 startActivity(intent);
                    }
                }).create();
       		 exitDialog.show();
        	}
         
        }
        catch (IOException localIOException)
        {
          while (true){
        	  localIOException.printStackTrace();
        	  Log.d("Holy shit,there is an Exception :",localIOException.getCause().toString());
          }
            
        }
    }
    void hackNow(String newOperatorName){
		//Telephony Informations.not sure whether to take it from getprop gsm.sim.operator or from the API,better from API as 
    	//2 options,take from getprop gsm.sim.operator options or from telephony API,second one seems to be safe,it might work with cmda too i guess.figure out.
		//TODO
		//Test on CDMA phones,detect the type, add the spn-conf.xml and see if it works
		 TelephonyManager tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		 String origOperatorCode = tel.getNetworkOperator();
		 
		 if(tel.getPhoneType()!=TelephonyManager.PHONE_TYPE_GSM){
			 //TODO
			 //Something needs to be done here,figure out later,dont have a CDMA phone now
		 }
		 if(checkIfHackable()){ 
			 //This is shot in the dark,im not sure wheter it works on all devices
			 //If works,this is by @r3dsm0k3,if not,i dont understand who did this. :P
			 String[] cmd1 = {"rm /system/etc/spn-config.xml"};
			 this.runAsRoot(cmd1);
		 }
			 
	     String xmlcmd = "echo \"<?xml version=\\\"1.0\\\" encoding=\\\"utf-8\\\"?>\n<spnOverrides>\n<spnOverride numeric=\\\""+origOperatorCode+"\\\" spn=\\\""+newOperatorName+"\\\"/>\n</spnOverrides>" + "\" > /system/etc/spn-conf.xml";
	
		 String[] cmd2 = {xmlcmd,"chmod 644 /system/etc/spn-conf.xml"};
		 this.runAsRoot(cmd2);
		 
		 AlertDialog noConfigInPlaceAlert = new AlertDialog.Builder(FakeCarrier.this)
         .setTitle("Do you believe in GOD ?")
         .setMessage("We think everything is proper cool and done,\n" +
         		"You need to restart your device to make sure.\n" +
         		"If it is not working,file a bug.\n" +
         		"Now lets try our luck with Reboot?,\n")
         .setPositiveButton("mmm,yeah", new DialogInterface.OnClickListener() {
             public void onClick(DialogInterface dialog, int whichButton) {
            	 String[] cmd3 = {"reboot"};	
            	 runAsRoot(cmd3);
             }
         })
         .setNegativeButton("no,later",null).create();
 		noConfigInPlaceAlert.show();
    }
    //TODO
   //Helper functions-(Aman,Clean this shit up,dont ya?) 
    void runAsRoot(String[] cmds){
    	try{
    		Process p = Runtime.getRuntime().exec("su");
    		
            DataOutputStream os = new DataOutputStream(p.getOutputStream());  
            os.writeBytes("mount -o rw,remount -t yaffs2 /dev/block/mtdblock3 /system\n");
            os.flush();
            
            for (String tmpCmd : cmds) {
                    os.writeBytes(tmpCmd+"\n");
                    os.flush();
            }           
            //os.writeBytes("exit\n");  
            //os.flush();
    	}
    	catch (IOException e) {
			e.printStackTrace();
		}   
    }
    public static boolean isDeviceRooted() {
    	
        String buildTags = android.os.Build.TAGS;
        if (buildTags != null && buildTags.contains("test-keys")) {
          return true;
        }
        // check if /system/app/Superuser.apk is present
        try {
          File file = new File("/system/app/Superuser.apk");
          if (file.exists()) {
            return true;
          }
        } catch (Throwable e1) {
          // ignore
        }

        return false;
    }
    boolean checkIfBackupExists(){
    	File file = new File("/system/etc/spn-config.xml.bkp");
    	return file.exists() ? true : false;
    }
    boolean checkIfHackable(){
    	File file = new File("/system/etc/spn-config.xml");
    	return file.exists() ? true : false;
    } 
    void backupMyConfig(){
    	String[] cmd = {"cp /system/etc/spn-conf.xml /system/etc/spn-conf.xml.bkp"};
    	this.runAsRoot(cmd);
    	Toast.makeText(this.getApplicationContext(), "Okay,We're done with the backing up.!", 3).show();
    }
    void restoreMyConfig(){
    	//permisisons need to be set correctly,dont know whether this works otherwise,we're not gonna take chances
    	//Two ways to do this,get the original file and copy it as spn-conf.xml or get the original operator name and create the file,either is fine.
    	String[] cmd = {"cat /system/etc/spn-conf.xml.bkp > /system/etc/spn-conf.xml","chmod 644 /system/etc/spn-conf.xml"};
    	this.runAsRoot(cmd);
		Toast.makeText(this.getApplicationContext(), "Okay,Restore Done,Now you are on your own.!", 3).show();
    } 

}